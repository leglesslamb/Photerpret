import json
from os.path import join, dirname
from os import environ
from watson_developer_cloud import VisualRecognitionV3

visual_recognition = VisualRecognitionV3("2016-05-20", api_key='f54e034b2dc900422f4894dded3258971d227c68')

with open(join(dirname(__file__), 'pepsi.zip'), 'rb') as pepsi, \
    open(join(dirname(__file__), 'cocacola.zip'), 'rb') as cocacola :
    print ("Uploading files...")
    print(json.dumps(visual_recognition.create_classifier('SoftDrinks', \
        pepsi_positive_examples=pepsi, \
        cocacola_positive_examples=cocacola), indent=2))