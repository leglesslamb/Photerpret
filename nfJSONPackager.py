import http.client, urllib.request, urllib.parse, urllib.error, base64, json
from semantics3 import Products
from watson_developer_cloud import VisualRecognitionV3
from os.path import join, dirname
from os import environ
###############################################
#### Update or verify the following values. ###
###############################################

# Replace the subscription_key string value with your valid subscription key.
subscription_key = '88a6a93962fe4995917f2a5d10471854'
visual_recognition = VisualRecognitionV3('2016-05-20', api_key='f54e034b2dc900422f4894dded3258971d227c68')

# Replace or verify the region.
#
# You must use the same region in your REST API call as you used to obtain your subscription keys.
# For example, if you obtained your subscription keys from the westus region, replace 
# "westcentralus" in the URI below with "westus".
#
# NOTE: Free trial subscription keys are generated in the westcentralus region, so if you are using
# a free trial subscription key, you should not need to change this region.
uri_base = 'eastus.api.cognitive.microsoft.com'

headers = {
    # Request headers.
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': subscription_key,
}

params = urllib.parse.urlencode({
    # Request parameters. The language setting "unk" means automatically detect the language.
    'language': 'unk',
    'detectOrientation ': 'true',
})

def correctString(string):
    '''(str) -> str
    Returns the string with some corrections missed by Azure
    '''
    # to be used for nutrition fact charts
    wrongs = [" I ", "0/0", " g", " mg ", "O/O", "%DV*"           , "tls", "\n ", "Seru"   , "Sal" , " 01", " 25" , "Dail Value", "O"]
    rights = [" 1 ", "%"  , "g" , "mg " , "%"  , "% Daily Value *", "t/s", "\n" , "Serving", "Sat.", " 0.", " 2.5", "Daily Value", "0"]
    
    # fix every single mistake Azure made
    for i in range(len(wrongs)):
        string = string.replace(wrongs[i], rights[i])
    
    # return
    return string

def stringToListIN(string):
    '''(str) -> dict of str'''
    # remove the b
    string = string[1:]
    # initiate default values and containers
    listOfPhrases = []
    redundant = False
    currentPhrase = ''

    # as long as the string isn't empty, keep cycling through the first letter
    while len(string) > 0:
        try:
            
            # word separator
            if string.index("\"boundingBox\"") == 0:
                listOfPhrases.append(currentPhrase.strip(" "))
                currentPhrase = ''
                string = string[13:]
            
            # word indicator
            elif string.index("\"text\"") == 0:
                currentPhrase += " " + string[8:8+string[8:].index('\"')]
                string = string[8+string[8:].index('\"'):]
            
            # pop anything else
            else:
                string = string[1:]

            # reset redundancy
            redundant = False
        
        # if there's a mistake, like index out of bounds, make it redundant
        # and break if redundant twice
        except:
            if redundant: break
            redundant = True

    # replaces every empty string with a line break, s
    for i in range(len(listOfPhrases)):
        if len(listOfPhrases[i]) == 0:
            listOfPhrases[i] = '\n'
    
    return listOfPhrases

def stringToListNF(string):
    '''(str) -> list of str
    Returns a list of lines from the json-parsed string
    '''
    
    # remove the b
    string = string[1:]
    # initiate default values and containers
    listOfPhrases = []
    currentPhrase = ''

    # as long as the string isn't empty, keep cycling through the first letter
    while len(string) > 0:
        if 1:
            # word separator
            if '\"boundingBox\"' in string:
                if string.index('\"boundingBox\"') == 0:
                    listOfPhrases.append(currentPhrase.strip(" "))
                    currentPhrase = ''
                    string = string[13:]
            
            # word indicator
            if '\"text\"' in string:
                if string.index('\"text\"') == 0:
                    currentPhrase += " " + string[8:8+string[8:].index('\"')]
                    string = string[7+string[8:].index('\"'):]
            
            # pop anything else
            string = string[1:]

    if (len(currentPhrase) > 0):
        listOfPhrases.append(currentPhrase.strip(" "))
    # replaces every empty string with a line break, s
    for i in range(len(listOfPhrases)):
        if len(listOfPhrases[i]) == 0:
            listOfPhrases[i] = '\n'
    
    return listOfPhrases

def firstInt(string):
    for i in range(len(string)):
        if string[i] in '0123456789': return i
        elif string[i] == 'l':
            if i > 0:
                if string[i-1] == ' ':
                    return i

def nflisttodictpackage(stringList):
    '''(list) -> dict
    Builds a dictionary with one key pointing to another dictionary containing
    the elements of the other nutrient pairs
    '''
    head = stringList[0]
    primaryDict = {}
    primaryDict[head] = {}
    if len(stringList) > 1:
        for i in range(1, len(stringList)):
            index = firstInt(stringList[i])
            if index is not None and index > 0:
                key = stringList[i][:index]
                item = stringList[i][index:]
            elif index == 0:
                key = "SUBSTANCE UNKNOWN"
                item = stringList[i]
            else:
                key = stringList[i]
                item = "AMOUNT UNKNOWN"
            primaryDict[head][key] = item
    return primaryDict

def jsonToURLList(jsonData):
    '''(json) -> list of url'''
    urlList = json.loads(jsonData)
    
    dictList = []

    for i in range(len(urlList)):
        dictList.append(urltodictpackage(urlList[i]))
    
    return json.dumps(dictList, indent=2)

def urltodictpackage(givenUrl):
    '''(url) -> dict of (dict of str)
    Converts a given URL to a dictionary of dictionary of str for packaging with
    other item properties
    '''

    body = "{'url':"+"'"+givenUrl+"'}"

    try:
        # Execute the REST API call and get the response.
        conn = http.client.HTTPSConnection(uri_base)
        conn.request("POST", "/vision/v1.0/ocr?%s" % params, body, headers)
        response = conn.getresponse()
        conn.close()
        data = response.read()
        
        # 'data' contains the JSON data. The following formats the JSON data for display.
        #parsed = json.loads(data)#.get("regions").get("text")
        #print (json.dumps(parsed, sort_keys=True, indent=2))
        #print(str(data))
        if "_NF_" in givenUrl:
            stringList = stringToListNF(str(data))
            stringList = ' '.join(stringList).replace('\n','  ')
            stringList = correctString(stringList)
            stringList = str.split(stringList, '  ')
            for i in range(len(stringList)-1, -1, -1):
                if stringList[i] == '':
                    stringList.pop(i)
                else:
                    stringList[i] = stringList[i].strip()
            stringDict = nflisttodictpackage(stringList)
            return stringDict

        elif "_IN_" in givenUrl:
            ingreDict = {}
            stringList = stringToListNF(data.decode('utf-8'))
            stringList = ' '.join(stringList).replace('\n','').strip(' ').replace('•',';')
            for i in range(len(stringList)):
                if (stringList[i:i+9] == "RELIGIOUS"):
                    ingreDict["Ingredients"] = stringList[13:i-1]
                    stringList = stringList[i:]
                    break
            for i in range(len(stringList)):
                if (stringList[i:i+8] == "REQUIRED"):
                    ingreDict["Religious Statement"] = stringList[21:i]
                    ingreDict["Required Allergen/GMO Statements"] = stringList[i + 71:]
                    break
            return ingreDict
        elif "_BC_" in givenUrl:
            stringList = stringToListNF(str(data))
            stringList = ''.join(stringList).replace('\n','')
            stringList = correctString(stringList)
            upcDict = {"UPC":stringList}
            upcDict["UPC Product Info"] = upcAPI(stringList)
            return upcDict
        else:
            stringList = stringToListNF(str(data))
            stringList = ' '.join(stringList).replace('\n','  ')
            stringList = correctString(stringList)
            stringList = str.split(stringList, '  ')
            for i in range(len(stringList)-1, -1, -1):
                if stringList[i] == '':
                    stringList.pop(i)
                else:
                    stringList[i] = stringList[i].strip()
            stringDict = nflisttodictpackage(stringList)
            stringDict["Colours"] = evaluateColours(givenUrl)
            return stringDict

    except Exception as e:
        print('Error:')
        print(e)

def upcAPI(upc):
    sem3 = Products(
        api_key="SEM37CDFD7F8EBACB182ECF21725E372C58E",
        api_secret="YmI5YjU0ZDk0NjE1Y2I5ZGQ3NWRmNTc2ZmY5YjQ4ZGM"
    )

    sem3.products_field("upc", upc)

    results = sem3.get_products().get("results")[0]
    upc_import = ["category","features","manufacturer","name","Container Type",
                  "features","recommended use","dimensions","capacity","food allergen statements",
                  "blob","food form","brand","height","length","width","size","weight"]
    upc_info = {}
    for key in upc_import:
        if (results.get(key) != None):
            upc_info[key] = results.get(key)

    upc_dict = {}

    for key in upc_info:
        if (key != "features"):
            upc_dict[key] = upc_info.get(key)
        else:
            for descKey in upc_info.get("features"):
                upc_dict[descKey] = upc_info.get("features").get(descKey)
    
    return(upc_dict)

def evaluateColours(fileURL):
    classes = visual_recognition.classify(parameters=json.dumps({
        'url':fileURL,
        'classifier_ids':['default'],
        'threshold': 0.65
    }))

    keyword = 'color'
    colouredProperties = classes['images'][0]['classifiers'][0]['classes']
    for i in range(len(colouredProperties)-1, -1, -1):
        if keyword not in colouredProperties[i]['class']:
            colouredProperties.pop(i)
    return colouredProperties
